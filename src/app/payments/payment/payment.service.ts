import {Injectable} from '@angular/core';
import {AuthService} from '../../users/services/auth.service';

@Injectable()
export class PaymentService {

  userId = '';

  constructor(private authService: AuthService) {
    if (authService.loggedIn()) {
      this.userId = authService.user._id;
    }
  }

  processPayment(token: any, amount) {
    const payment = {token, amount};
    return payment;
  }
}
