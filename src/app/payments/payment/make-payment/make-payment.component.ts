import {Component, OnInit, HostListener, EventEmitter, Output, Input} from '@angular/core';
import {PaymentService} from '../payment.service';
import {environment} from '../../../../environments/environment';

declare var StripeCheckout: any;

@Component({
  selector: 'app-make-payment',
  templateUrl: './make-payment.component.html',
  styleUrls: ['./make-payment.component.css']
})
export class MakePaymentComponent implements OnInit {

  handler: any;
  amount = 500; // === $5.00
  done = false;

  @Output() change: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private paymentSvc: PaymentService) {
  }

  ngOnInit() {
    this.handler = StripeCheckout.configure({
      key: environment.stripeKey,
      image: 'assets/img/rent.png',
      locale: 'auto',
      currency: 'eur',
      token: token => {
        this.paymentSvc.processPayment(token, this.amount);
      },
      closed: () => {
        this.done = true;
        this.change.emit(true);
      }
    });
  }

  handlePayment(amount) {
    this.amount = amount;
    if (this.paymentSvc.userId !== '') {
      this.handler.open({
        name: 'EasyCar',
        description: 'Valider votre réservation',
        amount: this.amount
      });
    } else {
      window.alert('Veuillez vous identifier!');
    }
  }

  @HostListener('window:popstate')
  onPopstate() {
    this.handler.close();
  }
}
