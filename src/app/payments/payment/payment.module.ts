import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MakePaymentComponent } from './make-payment/make-payment.component';
import {PaymentService} from './payment.service';
import {AuthService} from '../../users/services/auth.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    MakePaymentComponent
  ],
  exports: [
    MakePaymentComponent
  ],
  providers: [
    PaymentService,
    AuthService
  ]
})
export class PaymentModule { }
