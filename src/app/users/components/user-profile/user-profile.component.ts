import {Component, ElementRef, OnInit} from '@angular/core';
import {User} from '../../../models/user';
import {AuthService} from '../../services/auth.service';
import {UserService} from '../../services/user.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';
import {environment} from '../../../../environments/environment';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  user: User;
  uploadUrl = environment.serverUrl + '/uploads/profile';

  constructor(private authService: AuthService,
              private userService: UserService,
              private router: Router,
              private el: ElementRef,
              private flashMsgService: FlashMessagesService) {
    this.user = this.authService.user;
  }

  ngOnInit() {
  }

  update({value, valid}: { value: User, valid: boolean }) {
    if (valid) {
      this.userService.update(value).subscribe(
        (res) => {
          this.authService.user = this.user;
          this.flashMsgService.show('Modifications sauvegardées', {
            cssClass: 'alert alert-success', timeout: 4000
          });
          this.router.navigate(['/dashboard']);
        },
        (err) => {
          console.log(err);
          let errmsg = '';
          if (err.error.errmsg !== undefined) {
            if (err.error.errmsg.indexOf('duplicate key')) {
              errmsg = 'Cette adresse email est déjà utilisée';
            }
          } else {
            errmsg = err.error.message;
          }
          this.flashMsgService.show(errmsg, {
            cssClass: 'alert alert-danger', timeout: 4000
          });
        }
      );
    }
  }

  // the function which handles the file upload without using a plugin.
  upload() {
    // locate the file element meant for the file upload.
    const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
    // get the total amount of files attached to the file input.
    const fileCount: number = inputEl.files.length;
    // create a new formdata instance
    const formData = new FormData();
    // check if the filecount is greater than zero, to be sure a file was selected.
    if (fileCount > 0) { //  a file was selected
      // append the key name 'photo' with the first file in the element
      formData.append('photo', inputEl.files.item(0));
      this.userService.upload(formData).subscribe(
        (success) => {
          this.flashMsgService.show('Modifications sauvegardées', {
            cssClass: 'alert alert-success', timeout: 4000
          });
          this.user.photo = success.body.user.photo;
          this.authService.user.photo = success.body.user.photo;
          localStorage.setItem('user', JSON.stringify(success.body.user));
        },
        (error) => {
          console.log(error);
        });
    }
  }

  deletePicture() {
    this.userService.deletePicture().subscribe(
      (success) => {
        this.flashMsgService.show('Modifications sauvegardées', {
          cssClass: 'alert alert-success', timeout: 4000
        });
        this.user.photo = success.user.photo;
        this.authService.user.photo = success.user.photo;
        localStorage.setItem('user', JSON.stringify(success.user));
      },
      (error) => {
        console.log(error);
      });
  }
}
