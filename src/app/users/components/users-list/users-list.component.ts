import {Component, OnInit} from '@angular/core';
import {User} from '../../../models/user';
import {UserService} from '../../services/user.service';
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  // sorting
  key = 'name';
  reverse = false;
  p = 1;
  users: User[];

  constructor(private userService: UserService,
              private authService: AuthService,
              private router: Router,
              private flashMsgService: FlashMessagesService,
              config: NgbRatingConfig) {
    config.max = 5;
    this.userService.getUsers().subscribe(users => {
      this.users = users;
    });
  }

  ngOnInit() {
  }

  toggleActivation(user) {
    user.isActive = !user.isActive;
    this.userService.activateDeactivate(user).subscribe((res) => {
        this.flashMsgService.show('Modifications sauvegardées', {
          cssClass: 'alert alert-success', timeout: 4000
        });
      },
      (err) => {
        this.flashMsgService.show(err.error.message, {
          cssClass: 'alert alert-danger', timeout: 4000
        });
      });
  }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  changeRole(user) {
    if (user.role === 'admin') {
      user.role = 'member';
    } else {
      user.role = 'admin';
    }
    this.userService.changeRole(user).subscribe((res) => {
        this.flashMsgService.show('Modifications sauvegardées', {
          cssClass: 'alert alert-success', timeout: 4000
        });
      },
      (err) => {
        this.flashMsgService.show(err.error.message, {
          cssClass: 'alert alert-danger', timeout: 4000
        });
      });
  }
}
