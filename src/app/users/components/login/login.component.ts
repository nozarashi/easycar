import {AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {HttpErrorResponse} from '@angular/common/http';
import {FlashMessagesService} from 'angular2-flash-messages';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, OnDestroy, AfterViewInit {

  email: string;
  password: string;
  token: string;

  constructor(private authService: AuthService,
              private router: Router,
              private flashMsgService: FlashMessagesService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    document.querySelector('body').classList.add('body');
    document.querySelector('nav').classList.add('navbar-transparent');
  }

  ngOnDestroy() {
    document.querySelector('body').classList.add('body');
    document.querySelector('nav').classList.remove('navbar-transparent');
  }

  onSubmit() {

    if (!this.authService.loggedIn()) {
      this.authService.login(this.email, this.password)
        .subscribe(
          (res) => {
            this.flashMsgService.show('Bienvenue!', {
              cssClass: 'alert alert-success', timeout: 4000
            });
            this.token = res.headers.get('x-auth');
            localStorage.setItem('x-auth', this.token);
            localStorage.setItem('user', JSON.stringify(res.body));
            this.authService.user = res.body;
            this.router.navigate(['/dashboard']);
          },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              this.flashMsgService.show('Identifiants incorrects', {
                cssClass: 'alert alert-danger', timeout: 4000
              });
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          }
        );
    } else {
      console.log('already signed in', this.token);
    }
  }
}
