import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../../models/user';
import {environment} from '../../../environments/environment';

@Injectable()
export class UserService {

  getUsersUrl = environment.serverUrl + '/users';
  getMeUrl = environment.serverUrl + '/users/me';
  activateDeactivateUrl = environment.serverUrl + '/users/activate-deactivate';
  changeRoleUrl = environment.serverUrl + '/users/change-role';
  updateUserUrl = environment.serverUrl + '/users/me';
  changePasswordUrl = environment.serverUrl + '/users/change-password';
  uploadPictureUrl = environment.serverUrl + '/users/upload';
  notificationUrl = environment.serverUrl + '/users/notification';

  constructor(private http: HttpClient) {
  }

  getUsers() {
    return this.http.get(this.getUsersUrl)
      .map(res => res as User[]);
  }

  getMe() {
    return this.http.get(this.getMeUrl)
      .map(res => res as User);
  }
  activateDeactivate(user: User) {
    return this.http.patch(this.activateDeactivateUrl + '/' + user._id, {isActive: user.isActive}, {observe: 'response'})
      .shareReplay();
  }

  changeRole(user: User) {
    return this.http.patch(this.changeRoleUrl + '/' + user._id, {role: user.role}, {observe: 'response'})
      .shareReplay();
  }

  update(user: User) {
    return this.http.patch(this.updateUserUrl, user, {observe: 'response'})
      .shareReplay();
  }

  changePassword(data: any) {
    return this.http.patch(this.changePasswordUrl, data, {observe: 'response'})
      .shareReplay();
  }

  upload(formData: FormData) {
    return this.http.post(this.uploadPictureUrl, formData, {observe: 'response'})
      .map(res => res as any);
  }

  deletePicture() {
    return this.http.delete(this.uploadPictureUrl)
      .map(res => res as any);
  }

  deleteNotification(id: string) {
    return this.http.delete(this.notificationUrl + '/' + id);
  }
}
