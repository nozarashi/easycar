import {AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Travel} from '../../models/travel';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None,
  styles: ['.demo #search_places {height: 40px;border-radius: 0 5px 5px 0;}']
})
export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {
  departure: string;
  destination: string;
  travelDate: string;
  model;
  minDate = {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate()};

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  autoCompleteCallback1(selectedData: any) {
    this.departure = selectedData.data.vicinity;
  }

  autoCompleteCallback2(selectedData: any) {
    this.destination = selectedData.data.vicinity;
  }

  ngAfterViewInit() {
    document.querySelector('body').classList.add('body');
    document.querySelector('nav').classList.add('navbar-transparent');
  }

  ngOnDestroy() {
    document.querySelector('body').classList.remove('body');
    document.querySelector('nav').classList.remove('navbar-transparent');
  }

  researchTravels({value, valid}, event: Event) {
    event.preventDefault();
    let start: Date;
    /* if (this.departure === undefined && this.destination === undefined) {
       window.alert('Précisez un lieu de départ et une destination !');
       return;
     }*/
    if (this.model !== undefined) {
      start = new Date(this.model.year, this.model.month - 1, this.model.day, new Date().getHours());
      return this.router.navigate(['travels-list'], {queryParams: {dep: this.departure, dest: this.destination, start: start}});
    }
    return this.router.navigate(['travels-list'], {queryParams: {dep: this.departure, dest: this.destination}});

  }


}
