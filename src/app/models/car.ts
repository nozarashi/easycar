export interface Car {
  _id?: string;
  model?: string;
  make?: string;
  seating?: number;
  licensePlate?: string;
}
