export interface TravelType {
  price?: number;
  distanceMax?: number;
}
