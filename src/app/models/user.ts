export interface User {
  _id?: string;
  firstName?: string;
  lastName?: string;
  gender?: boolean;
  email?: string;
  password?: string;
  role?: string;
  photo?: string;
  isActive?: boolean;
  birth?: string;
  address?: {
    street?: string,
    city?: string,
    zip?: string,
    country?: string
  };
  phoneNumber?: string;
  rating?: number;
  notifications?: [
    {
      _id: {
        type: string
      },
      name: {
        type: string
      },
      photo: {
        type: string
      },
      message: {
        type: string
      },
      target: {
        type: string
      }
    }
    ];
}
