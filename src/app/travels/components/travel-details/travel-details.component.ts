import {Component, ElementRef, OnInit, AfterViewInit, ViewChild, ViewEncapsulation, Output, EventEmitter} from '@angular/core';
import {Travel} from '../../../models/travel';
import {ActivatedRoute, Router} from '@angular/router';
import {TravelService} from '../../services/travel.service';
import {AuthService} from '../../../users/services/auth.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {FormControl, Validators} from '@angular/forms';
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import {MakePaymentComponent} from '../../../payments/payment/make-payment/make-payment.component';
import {environment} from '../../../../environments/environment';

declare let google;

@Component({
  selector: 'app-travel-details',
  templateUrl: './travel-details.component.html',
  styleUrls: ['./travel-details.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TravelDetailsComponent implements OnInit {
  uploadUrl = environment.serverUrl + '/uploads/profile';
  Math: any;
  @ViewChild('stripe') stripe: MakePaymentComponent;
  @ViewChild('map') mapElement: ElementRef;


  travel: Travel = {
    departure: '',
    destination: '',
    departureAddress: '',
    destinationAddress: '',
    price: 0,
    date: new Date,
    placesAvailable: 0,
    car: {},
    user: {
      firstName: '',
      lastName: ''
    },
    comments: [{
      commentBody: '',
      rating: 0,
      commentUser: {
        firstName: '',
        lastName: ''
      },
      commentDate: 0
    }]
  };

  id: string;
  canBook = true;
  isPassenger = false;
  canCancel = false;
  ctrl = new FormControl(null, Validators.required);
  map: any;
  start = 'paris, fr';
  end = 'paris, fr';
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  comment = {
    commentBody: '',
    rating: 0,
    commentUser: null,
    commentDate: Date.now()
  };

  canPostComment = false;

  constructor(private travelService: TravelService,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute,
              private flashMsgService: FlashMessagesService,
              config: NgbRatingConfig) {
    this.Math = Math;
    config.max = 5;
    this.id = this.route.snapshot.params['id'];
    this.comment.commentUser = this.authService.user;
    this.travelService.getTravel(this.id).subscribe(res => {
      this.travel = res.travel;
      this.start = res.travel.departure;
      this.end = res.travel.destination;
      this.initMap();

      const loggedUser = this.authService.user;
      const index = !this.authService.loggedIn() ? -1 : this.travel.passengers.findIndex((user) => user._id === loggedUser._id);
      this.canBook = (index === -1 && (loggedUser === null ? true : this.travel.user._id !== loggedUser._id))
      && (new Date(this.travel.date).getTime() >= Date.now());
      this.isPassenger = index !== -1;
      this.canCancel = this.isPassenger && (new Date(this.travel.date).getTime() >= new Date().getTime());
      const index2 = !this.authService.loggedIn() ? -1 :
        this.travel.comments.findIndex((comment) => comment.commentUser._id === loggedUser._id);
      this.canPostComment = (new Date(this.travel.date).getTime() < new Date().getTime())
        && (this.isPassenger || (loggedUser._id === this.travel.user._id)) && (index2 === -1);
    });
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
  }


  initMap() {
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 5,
      center: {lat: 46.10, lng: 2.60}
    });

    this.directionsDisplay.setMap(this.map);

    this.directionsService.route({
      origin: this.start,
      destination: this.end,
      travelMode: 'DRIVING'
    }, (response, status) => {
      if (status === 'OK') {
        this.directionsDisplay.setDirections(response);
      }
      /*else {
             window.alert('Directions request failed due to ' + status);
           }*/
    });
  }

  book(id: string) {
    if (this.authService.loggedIn()) {
      this.travelService.book(id).subscribe(
        (res) => {
          this.flashMsgService.show('Réservation enregistrée', {
            cssClass: 'alert alert-success', timeout: 4000
          });
          this.router.navigate(['/dashboard']);
        },
        (err) => {
          this.flashMsgService.show(err.error.message, {
            cssClass: 'alert alert-danger', timeout: 4000
          });
          this.router.navigate(['/dashboard']);
        });
    } else {
      window.alert('Veuillez vous identifier!');
    }
  }

  cancelReservation(id: string) {
    if (this.authService.loggedIn()) {
      this.travelService.cancelReservation(id).subscribe(
        (res) => {
          this.flashMsgService.show('Réservation annulée', {
            cssClass: 'alert alert-success', timeout: 4000
          });
          this.router.navigate(['/dashboard']);
        },
        (err) => {
          this.flashMsgService.show(err.error.message, {
            cssClass: 'alert alert-danger', timeout: 4000
          });
          this.router.navigate(['/dashboard']);
        });
    } else {
      window.alert('Veuillez vous identifier!');
    }
  }


  addComment({value, valid}) {
    if (valid) {
      this.comment.rating = this.ctrl.value;
      this.comment.commentUser = this.comment.commentUser._id;
      if (this.authService.loggedIn()) {
        this.travelService.comment(this.travel._id, this.comment).subscribe(
          (res) => {
            this.flashMsgService.show('Commentaire soumis', {
              cssClass: 'alert alert-success', timeout: 4000
            });
            this.comment.commentUser = this.authService.user;
            this.travel.comments.unshift(this.comment);
            this.canPostComment = false;
          },
          (err) => {
            this.flashMsgService.show(err.error.message, {
              cssClass: 'alert alert-danger', timeout: 4000
            });
          });
      } else {
        window.alert('Veuillez vous identifier!');
      }
    }

  }

  calculate_age(dob) {
    const diff_ms = Date.now() - new Date(dob).getTime();
    const age_dt = new Date(diff_ms);

    return Math.abs(age_dt.getUTCFullYear() - 1970);
  }

}
