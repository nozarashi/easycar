import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TravelService} from '../../services/travel.service';
import {Travel} from '../../../models/travel';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-travels-list',
  templateUrl: './travels-list.component.html',
  styleUrls: ['./travels-list.component.css']
})
export class TravelsListComponent implements OnInit {
  uploadUrl = environment.serverUrl + '/uploads/profile';
  selectedItem: any = '';
  p = 1;
  travels: Travel[];
  model;
  time: number;
  minDate = {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate()};
  driver: string;

  key = 'name'; // set default
  reverse = false;

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  items2: any[] = [{id: 0, payload: {label: 'Tom'}},
    {id: 1, payload: {label: 'John'}},
    {id: 2, payload: {label: 'Lisa'}},
    {id: 3, payload: {label: 'Js'}},
    {id: 4, payload: {label: 'Java'}},
    {id: 5, payload: {label: 'c'}},
    {id: 6, payload: {label: 'vc'}}
  ];
  config2: any = {'placeholder': 'test', 'sourceField': ['payload', 'label']};

  onSelect(item: any) {
    this.selectedItem = item;
  }

  constructor(private router: Router, private route: ActivatedRoute, private travelService: TravelService) {
    this.time = 0;
  }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        //noinspection TypeScriptUnresolvedVariable
        this.travelService.travelQuery(params.dep, params.dest, params.start, undefined).subscribe(travels => {
          this.travels = travels;
        });
      });
  }

  sortTravel(f: { valid, value }, event) {
    event.preventDefault();
    let date;
    if (this.model !== undefined && this.time !== undefined) {
      date = new Date(this.model.year, this.model.month - 1, this.model.day);
      date.setHours(this.time);
    } else {
      date = new Date();
    }
    this.route
      .queryParams
      .subscribe(params => {
        //noinspection TypeScriptUnresolvedVariable
        this.travelService.travelQuery(params.dep, params.dest, date, undefined).subscribe(travels => {
          this.travels = this.driver === undefined ? travels
            : travels.filter(travel => travel.user.firstName.toLowerCase().includes(this.driver.trim().toLowerCase())
              || travel.user.lastName.toLowerCase().includes(this.driver.trim().toLowerCase()));
        });
      });

  }

}
