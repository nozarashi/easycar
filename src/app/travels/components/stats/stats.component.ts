import {Component, OnInit} from '@angular/core';
import {TravelService} from '../../services/travel.service';
import {UserService} from '../../../users/services/user.service';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {
  public nbUsers = 0;
  public allMonths: string[] = [
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Août',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre'
  ];

  public lineChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      xAxes: [{
        ticks: {
          fontSize: 9
        }
      }]
    }
  };

  public lineChartColors: Array<any> = [
    {
      borderColor: '#8e5ea2',
      fill: false
    }
  ];
  public lineChartLabels: string[];
  public lineChartData: any[] = [
    {data: [], label: 'Nombre de trajets-Six derniers mois'}
  ];
  public lineChartType = 'line';

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }],
      xAxes: [{
        ticks: {
          fontSize: 9
        }
      }]
    }
  };
  public barChartColors: Array<any> = [
    {
      backgroundColor: '#3cba9f'
    }
  ];

  public barChartLabels: string[];
  public barChartData: any[] = [
    {data: [], label: 'Trajets les plus populaires'}
  ];
  public barChartType = 'bar';
  public barChartLegend = true;
  public dates: any[];

  public doughnutChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    title: {
      display: true,
      text: 'Répartitions des membres du site par tranche d\'âges'
    }
  };
  public doughnutChartLabels = ['18-25 ans', '26-35 ans', 'Plus de 36 ans'];
  public doughnutChartData = [0, 0, 0];
  public doughnutChartType = 'doughnut';

  public pieChartLabels:string[] = ['Femme','Homme'];
  public pieChartData:number[] = [0, 0];
  public pieChartType:string = 'pie';


  public ready = false;

  constructor(private travelService: TravelService,
              private userService: UserService) {

    this.travelService.getNbTravelsPerMonth().subscribe(data => {
      this.dates = [];
      this.lineChartLabels = [];
      for (let i = 0; i < data.length; i++) {
        const m = data[i]._id.month - 1;
        const y = data[i]._id.year;
        if (this.dates.indexOf(new Date(y, m, 1)) === -1) {
          this.dates.push({date: new Date(y, m, 1), count: data[i].count});
        }
      }
      this.dates.sort((a, b) => a.date.getTime() - b.date.getTime());
      for (let i = 0; i < this.dates.length; i++) {
        this.lineChartLabels
          .push(this.allMonths[this.dates[i].date.getMonth()] + ' ' + this.dates[i].date.getFullYear());
        this.lineChartData[0].data.push(this.dates[i].count);
      }
    }, (err) => {
      console.log(err);
    });


    this.travelService.getPopularTravels().subscribe(data => {
      this.barChartLabels = [];
      for (let i = 0; i < data.length; i++) {
        const dep = data[i]._id.departure;
        const dest = data[i]._id.destination;
        this.barChartLabels.push(dep + '-' + dest);
        this.barChartData[0].data.push(data[i].count);
      }
    });


    this.userService.getUsers().subscribe(users => {
      this.nbUsers = users.length;
      for (let i = 0; i < users.length; i++) {
        const d = new Date(users[i].birth);
        const age = this.calculate_age(d);
        if (age <= 25) {
          this.doughnutChartData[0] += 1;
        } else if (age >= 26 && age <= 35) {
          this.doughnutChartData[1] += 1;
        } else {
          this.doughnutChartData[2] += 1;
        }
        if(users[i].gender)
        {
          this.pieChartData[1] += 1;
        }
        else
        {
            this.pieChartData[0] += 1;
        }
      }
      this.ready = true;
    });
  }

  ngOnInit() {
  }

  chartHovered(e: any) {

  }

  chartClicked(e: any) {

  }

  calculate_age(dob) {
    const diff_ms = Date.now() - new Date(dob).getTime();
    const age_dt = new Date(diff_ms);

    return Math.abs(age_dt.getUTCFullYear() - 1970);
  }
}
