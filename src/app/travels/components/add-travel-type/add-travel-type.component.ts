import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {TravelType} from '../../../models/travelType';
import {TravelService} from '../../services/travel.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';
import {ModalDismissReasons, NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

declare let google;

@Component({
  selector: 'app-add-travel-type',
  templateUrl: './add-travel-type.component.html',
  styleUrls: ['./add-travel-type.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddTravelTypeComponent implements OnInit {

  @ViewChild('contenttraveltype') contenttraveltype: ElementRef;
  private modalRef: NgbModalRef;
  closeResult: string;

  travelType: TravelType = {
    price: 0,
    distanceMax: 0,
  };



  constructor(private travelService: TravelService,
              private router: Router,
              private flashMsgService: FlashMessagesService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  open(content) {
    this.modalRef = this.modalService.open(content);

    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }




  addTravelType({value, valid}: { value: TravelType, valid: boolean }) {

    this.travelType.price = parseFloat(this.travelType.price.toString());
    this.modalRef.close();

    this.travelService.addTravelType(this.travelType).subscribe(
      (res) => {
        this.flashMsgService.show('Trajet-type sauvegardé', {
          cssClass: 'alert alert-success', timeout: 4000
        });
        this.router.navigate(['/travel-types']);
      },
      (err) => {
        this.flashMsgService.show(err.error.message, {
          cssClass: 'alert alert-danger', timeout: 4000
        });
        this.router.navigate(['/dashboard']);
      });
  }
}
