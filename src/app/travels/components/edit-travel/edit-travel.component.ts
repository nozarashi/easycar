import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Travel} from '../../../models/travel';
import {TravelService} from '../../services/travel.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Car} from '../../../models/car';
import {FlashMessagesService} from 'angular2-flash-messages';
import {TravelType} from '../../../models/travelType';

declare let google;

@Component({
  selector: 'app-edit-travel',
  templateUrl: './edit-travel.component.html',
  styleUrls: ['./edit-travel.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EditTravelComponent implements OnInit {
  model;
  // minDate = {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate()};
  minDate = {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate()};
  directionsService = new google.maps.DirectionsService;

  travel: Travel = {
    departure: '',
    destination: '',
    departureAddress: '',
    destinationAddress: '',
    averageTime: 0,
    distance: 0,
    price: 0,
    date: new Date,
    placesAvailable: 0,
    car: {},
  };
  cars: Car[];
  id: string;
  travelTypes: TravelType[];


  constructor(private travelService: TravelService,
              private router: Router,
              private route: ActivatedRoute,
              private flashMsgService: FlashMessagesService) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    this.travelService.getTravel(this.id).subscribe(res => {
      this.travel = res.travel;
      const date_ = new Date(res.travel.date);
      this.model = {
        year: date_.getFullYear(),
        month: date_.getMonth() + 1,
        day: date_.getDate()
      };
    });

    this.travelService.getCars().subscribe(cars => {
      this.cars = cars;
    });
  }


  setprice() {

    console.log((<HTMLInputElement>document.getElementById('distance')).value);

    this.travelService.getTravelTypeDistance(Number((<HTMLInputElement>document.getElementById('distance')).value))
      .subscribe(travelType => {
        this.travelTypes = travelType;
      });
    if (this.travelTypes.length === 0) {
      (<HTMLInputElement>document.getElementById('price')).max =
        (Number((<HTMLInputElement>document.getElementById('distance')).value) * 0.5).toFixed(2);
    }
    if (this.travelTypes.length !== 0) {
      (<HTMLInputElement>document.getElementById('price')).max =
        (Number((<HTMLInputElement>document.getElementById('distance')).value) * this.travelTypes[0].price).toFixed(2);
    }
    this.travelTypes = [];
  }

  autoCompleteCallback1(selectedData: any) {
    console.log(selectedData.data.vicinity);
    console.log(selectedData.data.formatted_address);
    const result = selectedData.data.vicinity;
    const substring = ',';
    let getid;
    if (result.indexOf(substring) >= 0) {
      const resultid = result.split(',');
      getid = resultid [resultid.length - 1];
    } else {
      getid = result;
    }
    console.log(getid);
    this.travel.departure = getid;
    this.travel.departureAddress = selectedData.data.formatted_address;
    const request = {
      origin: this.travel.departureAddress,
      destination: this.travel.destinationAddress,
      travelMode: 'DRIVING'
    };
    if (this.travel.destinationAddress !== '') {
      this.directionsService.route(request, function (response, status) {
        if (status === 'OK') {
          (<HTMLInputElement>document.getElementById('averageTime')).value =
            (response.routes[0].legs[0].distance.value / 1000).toFixed(0);
          (<HTMLInputElement>document.getElementById('distance')).value =
            (response.routes[0].legs[0].duration.value / 60).toFixed(1);
          (<HTMLInputElement>document.getElementById('price')).max =
            ((response.routes[0].legs[0].duration.value / 60) * 0.1).toFixed(2);
        } else {
          alert('Unable to find the distance via road.');
        }
      });
    }
  }

  autoCompleteCallback2(selectedData: any) {
    console.log(selectedData.data.vicinity);
    console.log(selectedData.data.formatted_address);
    const result = selectedData.data.vicinity;
    const substring = ',';
    let getid;
    if (result.indexOf(substring) >= 0) {
      const resultid = result.split(',');
      getid = resultid [resultid.length - 1];
    } else {
      getid = result;
    }
    console.log(getid);
    this.travel.destination = getid;
    this.travel.destinationAddress = selectedData.data.formatted_address;
    const request = {
      origin: this.travel.departureAddress,
      destination: this.travel.destinationAddress,
      travelMode: 'DRIVING'
    };
    if (this.travel.departureAddress !== '') {
      this.directionsService.route(request, function (response, status) {
        if (status === 'OK') {
          (<HTMLInputElement>document.getElementById('averageTime')).value =
            (response.routes[0].legs[0].distance.value / 1000).toFixed(0);
          (<HTMLInputElement>document.getElementById('distance')).value =
            (response.routes[0].legs[0].duration.value / 60).toFixed(1);
          (<HTMLInputElement>document.getElementById('price')).max =
            ((response.routes[0].legs[0].duration.value / 60) * 0.1).toFixed(2);
        } else {
          alert('Unable to find the distance via road.');
        }
      });
    }
  }

  editTravel({value, valid}: { value: Travel, valid: boolean }) {
    value.date = new Date(this.model.year + '-' + this.model.month + '-' + this.model.day);
    this.travel.averageTime = Number((<HTMLInputElement>document.getElementById('averageTime')).value);
    this.travel.distance = Number((<HTMLInputElement>document.getElementById('distance')).value);
    value.averageTime = Number((<HTMLInputElement>document.getElementById('averageTime')).value);
    value.distance = Number((<HTMLInputElement>document.getElementById('distance')).value);
    if (value.departure == null) {
      value.departure = this.travel.departure;
      value.departureAddress = this.travel.departureAddress;
      value.destination = this.travel.destination;
      value.destinationAddress = this.travel.destinationAddress;
    }
    console.log(value);
    console.log(this.id);
    if (valid) {
      this.travelService.editTravel(this.id, value).subscribe(
        (res) => {
          this.flashMsgService.show('Trajet modifié', {
            cssClass: 'alert alert-success', timeout: 4000
          });
          this.router.navigate(['/travels']);
        },
        (err) => {
          this.flashMsgService.show(err.error.message, {
            cssClass: 'alert alert-danger', timeout: 4000
          });
          this.router.navigate(['/dashboard']);
        });
    }
  }
}
