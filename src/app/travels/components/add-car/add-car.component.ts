import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';
import {Car} from '../../../models/car';
import {TravelService} from '../../services/travel.service';
import {ModalDismissReasons, NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-car',
  templateUrl: 'add-car.component.html',
  styleUrls: ['add-car.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddCarComponent implements OnInit {

  @ViewChild('contentcar') contentcar: ElementRef;
  private modalRef: NgbModalRef;
  closeResult: string;

  car: Car = {
    model: '',
    make: '',
    seating: 1,
    licensePlate: '',
  };

  constructor(private carService: TravelService,
              private router: Router,
              private flashMsgService: FlashMessagesService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  open(content) {
    this.modalRef = this.modalService.open(content);
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  addCar({value, valid}: { value: Car, valid: boolean }) {
    this.modalRef.close();
    this.carService.addCar(this.car).subscribe(
      (res) => {
        this.flashMsgService.show('Voiture sauvegardé', {
          cssClass: 'alert alert-success', timeout: 4000
        });

        this.router.navigate(['/cars']);
      },
      (err) => {
        console.log(err);
        let errmsg = '';
        if (err.error.errmsg !== undefined) {
          if (err.error.errmsg.indexOf('duplicate key')) {
            errmsg = 'Plaque de la voiture deja existante';
          }
        } else {
          errmsg = err.error.message;
        }
        this.flashMsgService.show(errmsg, {
          cssClass: 'alert alert-danger', timeout: 4000
        });
        this.router.navigate(['/cars']);
      });
  }
}
