import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {TravelService} from '../../services/travel.service';
import {TravelType} from '../../../models/travelType';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';

@Component({
  selector: 'app-travel-types',
  templateUrl: './travel-types.component.html',
  styleUrls: ['./travel-types.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TravelTypesComponent implements OnInit {
  travels: TravelType[];
  Math: any;
  p = 1;

  constructor(private flashMsgService: FlashMessagesService,
              private router: Router,
              private travelService: TravelService) {
    this.Math = Math;
    
    this.travelService.getTravelTypes().subscribe(travels => {
      this.travels = travels;
    });
  }

  ngOnInit() {
  }

  deleteTravelType(id: string) {
    
    this.travelService.deleteTravelType(id).subscribe(
      (res) => {
        window.location.reload(true);
        this.flashMsgService.show('Trajet Type supprimé', {
          cssClass: 'alert alert-success', timeout: 4000
        });
      },
      (err) => {
        this.flashMsgService.show(err.error.message, {
          cssClass: 'alert alert-danger', timeout: 4000
        });
        this.router.navigate(['/dashboard']);
      }
    );
  }

}
