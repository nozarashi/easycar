import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/shareReplay';
import 'rxjs/add/operator/map';
import {TravelType} from '../../models/travelType';
import {Travel} from '../../models/travel';
import {Car} from '../../models/car';
import {AuthService} from '../../users/services/auth.service';
import {environment} from '../../../environments/environment';

@Injectable()
export class TravelService {
  getTravelsTypesUrl = environment.serverUrl + '/travel-types';
  travelsUrlRoot = environment.serverUrl + '/travels';
  getTravelsUrl: string = this.travelsUrlRoot + '/allUserAfter';
  getCarsUrl = environment.serverUrl + '/cars';
  travelQuerySpecificAfter = environment.serverUrl + '/travels/specificAfter';
  travelQuerySpecificAfterDate = environment.serverUrl + '/travels/specificAfterDate';
  travelQuerySpecificInBetween = environment.serverUrl + '/travels/specificInbetween';
  travelQueryDepartureAfter = environment.serverUrl + '/travels/departureAfter';
  travelQueryDepartureAfterDate = environment.serverUrl + '/travels/departureAfterDate';
  travelQueryDepartureInBetween = environment.serverUrl + '/travels/departureInbetween';
  travelQueryDestinationAfter = environment.serverUrl + '/travels/destinationAfter';
  travelQueryDestinationAfterDate = environment.serverUrl + '/travels/destinationAfterDate';
  travelQueryDestinationInBetween = environment.serverUrl + '/travels/destinationInbetween';
  travelQueryDate = environment.serverUrl + '/travels/dateafter';
  travelPerMonth = environment.serverUrl + '/travels/travels-per-month';
  popularTravels = environment.serverUrl + '/travels/popular-travels';

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  getTravelTypes() {
    return this.http.get(this.getTravelsTypesUrl)
      .map(res => res as TravelType[]);
  }

  getTravels() {
    return this.http.get(this.getTravelsUrl)
      .map(res => res as Travel[]);
  }

  getAllTravels() {
    return this.http.get(this.travelsUrlRoot + '/allUser')
      .map(res => res as Travel[]);
  }

  getTravelsAsPassenger() {
    return this.http.get(this.travelsUrlRoot + '/asPassenger')
      .map(res => res as Travel[]);
  }

  getTravelsHistoryAsPassenger() {
    return this.http.get(this.travelsUrlRoot + '/history-asPassenger')
      .map(res => res as Travel[]);
  }

  addTravelType(travelType: TravelType) {
    return this.http.post<TravelType>(this.getTravelsTypesUrl, travelType, {observe: 'response'})
      .shareReplay();
  }

  addTravel(travel: Travel) {
    return this.http.post<TravelType>(this.travelsUrlRoot, travel, {observe: 'response'})
      .shareReplay();
  }

  getTravelType(id: string) {
    return this.http.get(this.getTravelsTypesUrl + '/' + id)
      .map(res => res as any);
  }

  getTravelTypeDistance(distance: number) {
    console.log(distance);
    return this.http.get(this.getTravelsTypesUrl + '/distance/' + distance)
      .map(res => res as any);
  }


  editTravel(id: string, travel: Travel) {
    return this.http.patch<Travel>(this.travelsUrlRoot + '/edit/' + id, travel, {observe: 'response'})
      .shareReplay();
  }

  editTravelType(id: string, travelType: TravelType) {
    return this.http.patch<TravelType>(this.getTravelsTypesUrl + '/edit/' + id, travelType, {observe: 'response'})
      .shareReplay();
  }

  getCars() {
    return this.http.get(this.getCarsUrl)
      .map(res => res as Car[]);
  }

  getTravel(id: string) {
    return this.http.get(this.travelsUrlRoot + '/' + id)
      .map(res => res as any);
  }

  deleteTravelType(id: string) {
    return this.http.delete(this.getTravelsTypesUrl + '/' + id)
      .map(res => res as TravelType);
  }

  deleteTravel(id: string) {
    return this.http.delete(this.travelsUrlRoot + '/' + id)
      .map(res => res as Travel);
  }

  undeleteTravel(id: string) {
    return this.http.delete(this.travelsUrlRoot + '/undelete/' + id)
      .map(res => res as Travel);
  }

  book(id: string) {
    return this.http.post(this.travelsUrlRoot + '/book/' + id, this.authService.user._id, {observe: 'response'})
      .shareReplay();
  }

  cancelReservation(id: string) {
    return this.http.delete(this.travelsUrlRoot + '/book/' + id, {observe: 'response'})
      .shareReplay();
  }

  comment(id: string, comment) {
    return this.http.post(this.travelsUrlRoot + '/comment/' + id, comment, {observe: 'response'})
      .shareReplay();
  }

  travelQuery(departure: string, destination: string, start: Date, end: Date) {
    if (start !== undefined && end !== undefined) {
      if (departure === undefined) {
        return this.http.get(this.travelQueryDestinationInBetween + `/${destination}/${start}/${end}`)
          .map(res => res as Travel[]);
      }
      if (destination === undefined) {
        return this.http.get(this.travelQueryDepartureInBetween + `/${departure}/${start}/${end}`)
          .map(res => res as Travel[]);
      }
      return this.http.get(this.travelQuerySpecificInBetween + `/${departure}/${destination}/${start}/${end}`)
        .map(res => res as Travel[]);
    } else if (start !== undefined && end === undefined) {
      if (departure === undefined && destination === undefined) {
        return this.http.get(this.travelQueryDate + `/${start}`)
          .map(res => res as Travel[]);
      }
      if (departure === undefined) {
        return this.http.get(this.travelQueryDestinationAfterDate + `/${destination}/${start}`)
          .map(res => res as Travel[]);
      }
      if (destination === undefined) {
        return this.http.get(this.travelQueryDepartureAfterDate + `/${departure}/${start}`)
          .map(res => res as Travel[]);
      }
      return this.http.get(this.travelQuerySpecificAfterDate + `/${departure}/${destination}/${start}`)
        .map(res => res as Travel[]);
    } else if (start === undefined && end === undefined) {
      if (departure === undefined) {
        return this.http.get(this.travelQueryDestinationAfter + `/${destination}`)
          .map(res => res as Travel[]);
      }
      if (destination === undefined) {
        return this.http.get(this.travelQueryDepartureAfter + `/${departure}`)
          .map(res => res as Travel[]);
      }
      return this.http.get(this.travelQuerySpecificAfter + `/${departure}/${destination}`)
        .map(res => res as Travel[]);
    }
  }

  getNbTravelsPerMonth() {
    return this.http.get(this.travelPerMonth)
      .map(res => res as any);
  }

  getPopularTravels() {
    return this.http.get(this.popularTravels)
      .map(res => res as any);
  }

  deleteCar(id: string) {
    return this.http.delete(this.getCarsUrl + '/' + id)
        .map(res => res as Car);

  }

  addCar(car: Car) {
    return this.http.post<Car>(this.getCarsUrl, car, {observe: 'response'})
        .shareReplay();
  }
}
