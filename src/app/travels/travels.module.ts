import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AddCarComponent} from './components/add-car/add-car.component';
import {AddTravelTypeComponent} from './components/add-travel-type/add-travel-type.component';
import {AddTravelComponent} from './components/add-travel/add-travel.component';
import {TravelsComponent} from './components/travels/travels.component';
import {CarsComponent} from './components/cars/cars.component';
import {EditTrajetTypeComponent} from './components/edit-trajet-type/edit-trajet-type.component';
import {TravelTypesComponent} from './components/travel-types/travel-types.component';
import {TravelsListComponent} from './components/travels-list/travels-list.component';
import {EditTravelComponent} from './components/edit-travel/edit-travel.component';
import {TravelDetailsComponent} from './components/travel-details/travel-details.component';
import {Ng2OrderModule} from 'ng2-order-pipe';
import {Ng2SearchPipeModule} from 'ng2-search-filter/index';
import {FlashMessagesModule, FlashMessagesService} from 'angular2-flash-messages';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {JwtModule} from '@auth0/angular-jwt';
import {NgxPaginationModule} from 'ngx-pagination';
import {Ng4GeoautocompleteModule} from 'ng4-geoautocomplete';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';
import {TravelService} from './services/travel.service';
import {AuthGuard} from '../guards/auth.guard';
import {AuthService} from '../users/services/auth.service';
import {TestComponent} from './components/test/test.component';
import { StatsComponent } from './components/stats/stats.component';
import {ChartsModule} from 'ng2-charts';
import {PaymentModule} from '../payments/payment/payment.module';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('x-auth');
        },
        headerName: 'x-auth',
        authScheme: '',
        whitelistedDomains: ['localhost:5000']
      }
    }),
    FlashMessagesModule,
    NgxPaginationModule,
    Ng4GeoautocompleteModule.forRoot(),
    Ng2SearchPipeModule,
    Ng2OrderModule,
    RouterModule,
    ChartsModule,
    PaymentModule
  ],
  declarations: [
    TestComponent,
    TravelsComponent,
    TravelTypesComponent,
    TravelsListComponent,
    AddTravelTypeComponent,
    TravelDetailsComponent,
    AddTravelComponent,
    CarsComponent,
    AddCarComponent,
    EditTravelComponent,
    EditTrajetTypeComponent,
    DashboardComponent,
    StatsComponent
  ],
  providers: [
    AuthService,
    TravelService,
    AuthGuard,
    FlashMessagesService
  ]
})

export class TravelsModule {
}

