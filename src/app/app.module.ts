import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {JwtModule} from '@auth0/angular-jwt';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FlashMessagesModule, FlashMessagesService} from 'angular2-flash-messages';
import {Ng4GeoautocompleteModule} from 'ng4-geoautocomplete';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import {Ng2OrderModule} from 'ng2-order-pipe';

import {AppComponent} from './app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {LoginComponent} from './users/components/login/login.component';
import {RegisterComponent} from './users/components/register/register.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';

import {AuthService} from './users/services/auth.service';

import {AuthGuard} from './guards/auth.guard';
import {AdminGuard} from './guards/admin.guard';
import {DashboardComponent} from './travels/components/dashboard/dashboard.component';
import {TravelTypesComponent} from './travels/components/travel-types/travel-types.component';
import {TravelService} from './travels/services/travel.service';
import {TravelsComponent} from './travels/components/travels/travels.component';
import {TravelDetailsComponent} from './travels/components/travel-details/travel-details.component';
import {CarsComponent} from './travels/components/cars/cars.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {EditTravelComponent} from './travels/components/edit-travel/edit-travel.component';
import {EditTrajetTypeComponent} from './travels/components/edit-trajet-type/edit-trajet-type.component';
import {TravelsListComponent} from './travels/components/travels-list/travels-list.component';
import {TravelsModule} from './travels/travels.module';
import {UsersModule} from './users/users.module';
import {HomeComponent} from './components/home/home.component';
import {UsersListComponent} from './users/components/users-list/users-list.component';
import {StatsComponent} from './travels/components/stats/stats.component';
import {ChartsModule} from 'ng2-charts';
import {PaymentModule} from './payments/payment/payment.module';
import {MakePaymentComponent} from './payments/payment/make-payment/make-payment.component';
import {UserProfileComponent} from './users/components/user-profile/user-profile.component';
import {ChangePasswordComponent} from './users/components/change-password/change-password.component';

// Create Routes
const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'travel-types', component: TravelTypesComponent, canActivate: [AuthGuard]},
  {path: 'travels', component: TravelsComponent, canActivate: [AuthGuard]},
  {path: 'cars', component: CarsComponent, canActivate: [AuthGuard]},
  {path: 'travel-edit/:id', component: EditTravelComponent, canActivate: [AuthGuard]},
  {path: 'travel-details/:id', component: TravelDetailsComponent},
  {path: 'travel-type-edit/:id', component: EditTrajetTypeComponent, canActivate: [AuthGuard]},
  {path: 'travels-list', component: TravelsListComponent},
  {path: 'users', component: UsersListComponent, canActivate: [AdminGuard]},
  {path: 'profile', component: UserProfileComponent, canActivate: [AuthGuard]},
  {path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthGuard]},
  {path: 'stats', component: StatsComponent, canActivate: [AdminGuard]},
  {path: 'checkout', component: MakePaymentComponent, canActivate: [AuthGuard]},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('x-auth');
        },
        headerName: 'x-auth',
        authScheme: '',
        whitelistedDomains: ['localhost:5000']
      }
    }),
    FlashMessagesModule,
    NgxPaginationModule,
    Ng4GeoautocompleteModule.forRoot(),
    Ng2SearchPipeModule,
    Ng2OrderModule,
    TravelsModule,
    UsersModule,
    PaymentModule,
    ChartsModule
  ],
  providers: [
    AuthService,
    TravelService,
    AuthGuard,
    AdminGuard,
    FlashMessagesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
